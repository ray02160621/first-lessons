package main

import (
	"fmt"
	"math"
)

func main() {
	for i := 100; i < 1000; i++ {
		x := i / 100
		y := (i / 10) % 10
		z := i % 10

		if math.Pow(float64(x), 3)+math.Pow(float64(y), 3)+math.Pow(float64(z), 3) == float64(i) {
			fmt.Println(i)
		}
	}
}
